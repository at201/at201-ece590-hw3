//
//  StudentTableViewController.swift
//  at201-ece590-hw32
//
//  Created by Adam on 9/20/16.
//  Copyright © 2016 Adam. All rights reserved.
//

import UIKit

class StudentTableViewController: UITableViewController {
    
    var students = [Student]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = editButtonItem()
        loadTeam()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getAdam() -> Student{
        let image = UIImage(named: "adam")!
        let name: String = "Adam Tache"
        let from: String = "Pembroke Pines, Florida"
        let program: String = "Undergraduate"
        let years : Int = 3
        let degree: String = "B.S.E. Electrical & Computer Engineering, B.S. Computer Science, and Mathematics minor"
        let gender: Student.Gender = .Male
        let interests: [String] = ["software engineering", "virtual and augmented reality", "drones and robotics", "photography and filmmaking"]
        let languages: [String] = ["Java, Python, C, C++, MATLAB, Javascript, HTML, CSS"]
        let student = Student(im: image, n: name, loc: from, py: years, d: degree, l: languages, i: interests, g: gender, p: program)
        return student
    }
    
    func getMatt() -> Student{
        let image = UIImage(named: "matt")!
        let name: String = "Matt Battles"
        let from: String = "New Jersey"
        let program: String = "Undergraduate"
        let years : Int = 4
        let degree: String = "B.S.E. Electrical & Computer Engineering, B.S. Computer Science, and finance minor"
        let gender: Student.Gender = .Male
        let interests: [String] = ["software engineering"]
        let languages: [String] = ["Java, C#, C, MATLAB, Python"]
        let student = Student(im: image, n: name, loc: from, py: years, d: degree, l: languages, i: interests, g: gender, p: program)
        return student
    }
    
    func getSteven() -> Student{
        let image = UIImage(named: "steven")!
        let name: String = "Steven Katsohirakis"
        let from: String = "Utah"
        let program: String = "Undergraduate"
        let years : Int = 4
        let degree: String = "B.S.E. Electrical & Computer Engineering, B.S. Computer Science"
        let gender: Student.Gender = .Male
        let interests: [String] = ["software engineering", "application development"]
        let languages: [String] = ["Java, C, Swift, JavaScript"]
        let student = Student(im: image, n: name, loc: from, py: years, d: degree, l: languages, i: interests, g: gender, p: program)
        return student
    }
    
    func loadTeam() {
        students += [getAdam(), getMatt(), getSteven()]
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + students.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row > 0){
            let cell = tableView.dequeueReusableCellWithIdentifier("StudentTableViewCell", forIndexPath: indexPath) as! StudentTableViewCell
            
            let student = students[indexPath.row - 1]
            cell.nameLabel.text = student.name
            cell.photoImageView.image = student.image
            
            return cell
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("TeamTableViewCell", forIndexPath: indexPath) as! TeamTableViewCell
        return cell
    }
    
    @IBAction func unwindToStudentList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.sourceViewController as? StudentViewController, thisStudent = sourceViewController.student {
            if let selectedIndexPath = tableView.indexPathForSelectedRow{
                students[selectedIndexPath.row - 1] = thisStudent
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            }
            else{
                let newRow = students.count + 1
                let newIndexPath = NSIndexPath(forRow: newRow, inSection: 0)
                students.append(thisStudent)
                tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
            }
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if(indexPath == 0){
            return false;
        }
        return true
    }
    
    override func tableView(tableView: UITableView, shouldIndentWhileEditingRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if(indexPath == 0){
            return false;
        }
        return true;
    }
    
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        if(indexPath == 0){
            return .None
        }
        return .Delete
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            students.removeAtIndex(indexPath.row - 1)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail" {
            let studentDetailViewController = segue.destinationViewController as! DisplayViewController
            
            if let selectedStudentCell = sender as? StudentTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedStudentCell)!
                studentDetailViewController.student = students[indexPath.row - 1]
            }
        }
    }
    
}