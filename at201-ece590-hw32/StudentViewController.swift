//
//  BasicViewController.swift
//  at201-ece590-hw2
//
//  Created by Adam on 9/10/16.
//  Copyright © 2016 Adam. All rights reserved.
//

import UIKit

class StudentViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var profileView: UIView!
    
    var nameLabelView: UILabel! = UILabel()
    var nameTextView: UITextField! = UITextField()
    
    var locationLabelView: UILabel! = UILabel()
    var locationTextView: UITextField! = UITextField()
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    var genderLabelView: UILabel! = UILabel()
    var genderSwitch: UISwitch = UISwitch()
    var genderSwitchLabel: UITextField! = UITextField()
    
    var programLabelView: UILabel! = UILabel()
    var programTextView: UITextField! = UITextField()
    
    var yearsProgramLabelView: UILabel! = UILabel()
    var yearsProgramSliderView: UISlider! = UISlider()
    var yearSliderLabel: UITextField! = UITextField()
    
    var degreeLabelView: UILabel! = UILabel()
    var degreeTextView: UITextField! = UITextField()
    
    var languagesLabelView: UILabel! = UILabel()
    var languagesTextView: UITextField! = UITextField()
    
    var interestsLabelView: UILabel! = UILabel()
    var interestsTextView: UITextField! = UITextField()
    
    var descriptionLabel: UILabel! = UILabel()
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBAction func cancelButton(sender: UIBarButtonItem) {
        let isPresentingInAddMealMode = presentingViewController is UINavigationController
        
        if isPresentingInAddMealMode {
            dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            navigationController!.popViewControllerAnimated(true)
        }
    }
    
    var student : Student?
    
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    var screenWidth: CGFloat = 0.0
    var screenHeight: CGFloat = 0.0
    
    var labelWidth : CGFloat = 125.0
    var labelHeight : CGFloat = 30.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        profileView = self.view
        if(student == nil){
            setupEmptyFields()
        }
        else{
            setupCurrStudent()
        }
        setupProfileView()
        nameTextView!.delegate = self
        checkValidStudentName()
    }
    
    func setupEmptyFields(){
        setupName("Johny Appleseed")
        setupLocation("Weston, Florida")
        setupProgram("Masters")
        setupDegree("Electrical Engineering")
        setupGender()
        setupLanguages("Java, C, C++")
        setupInterests("Urban exploration, skiing")
    }
    
    func setupCurrStudent(){
        setupName(student!.name)
        nameTextView.text = student!.name
        setupLocation(student!.location)
        locationTextView.text = student!.location
        setupProgram(student!.program)
        programTextView.text = student!.program
        setupDegree(student!.degree)
        degreeTextView.text = student!.degree
        setupGender()
        setupYearsInProgram()
        yearsProgramSliderView.value = Float(student!.yearInProgram)
        yearSliderLabel.text = String(student!.yearInProgram)
        genderSwitch.on = student!.gender == "male"
        genderSwitchLabel.text = student!.gender == "male" ? "Male" : "Female"
        setupLanguages(student!.languages.joinWithSeparator(", "))
        languagesTextView.text = student!.languages.joinWithSeparator(", ")
        setupInterests(student!.interests.joinWithSeparator(", "))
        interestsTextView.text = student!.interests.joinWithSeparator(", ")
        photoImageView.image = student!.image
    }
    
    func setupProfileView(){
        profileView.addSubview(nameLabelView)
        profileView.addSubview(nameTextView)
        
        profileView.addSubview(locationLabelView)
        profileView.addSubview(locationTextView)
        
        profileView.addSubview(yearSliderLabel)
        profileView.addSubview(yearsProgramLabelView)
        profileView.addSubview(yearsProgramSliderView)
        
        profileView.addSubview(programLabelView)
        profileView.addSubview(programTextView)
        
        profileView.addSubview(genderLabelView)
        profileView.addSubview(genderSwitch)
        profileView.addSubview(genderSwitchLabel)
        
        profileView.addSubview(languagesLabelView)
        profileView.addSubview(languagesTextView)
        
        profileView.addSubview(interestsLabelView)
        profileView.addSubview(interestsTextView)
        
        profileView.addSubview(degreeLabelView)
        profileView.addSubview(degreeTextView)
        
        profileView.addSubview(descriptionLabel)
    }
    
    func createLabelView(text: String, dx: CGFloat, dy: CGFloat, labelWidth: CGFloat, labelHeight: CGFloat) -> UILabel{
        let labelView = UILabel(frame: CGRectMake(dx, dy, labelWidth, labelHeight))
        labelView.hidden = false
        labelView.backgroundColor = UIColor.whiteColor()
        labelView.layer.borderColor = UIColor.blackColor().CGColor
        labelView.text = text
        labelView.adjustsFontSizeToFitWidth = true
        return labelView
    }
    
    func createTextView(text: String, dx: CGFloat, dy: CGFloat, labelWidth: CGFloat, labelHeight: CGFloat) -> UITextField{
        let textView = UITextField(frame: CGRectMake(dx, dy, labelWidth*1.5, labelHeight))
        textView.hidden = false
        textView.backgroundColor = UIColor.whiteColor()
        textView.layer.borderColor = UIColor.blackColor().CGColor
        textView.placeholder = text
        textView.tintColor = UIColor.blackColor()
        return textView
    }
    
    func createSliderView(min: Float, max: Float, dx: CGFloat, dy: CGFloat, sliderWidth: CGFloat, sliderHeight: CGFloat) -> UISlider{
        let slider : UISlider = UISlider()
        slider.minimumTrackTintColor = UIColor.blackColor()
        slider.minimumValue = min
        slider.maximumValue = max
        slider.continuous = true
        slider.tintColor = UIColor.blackColor()
        slider.frame = CGRectMake(dx, dy, sliderWidth, sliderHeight)
        return slider
    }
    
    func setupName(text: String){
        let dx : CGFloat = 20.0
        let dy : CGFloat = 65.0
        
        nameLabelView = createLabelView("Name", dx: dx, dy: dy, labelWidth: labelWidth, labelHeight: labelHeight)
        nameTextView = createTextView(text, dx: dx + 10 + labelWidth, dy: dy, labelWidth: 100, labelHeight: 30)
    }
    
    func setupLocation(text: String){
        let dx : CGFloat = 20.0
        let dy : CGFloat = 105.0
        
        locationLabelView = createLabelView("Location", dx: dx, dy: dy, labelWidth: labelWidth, labelHeight: labelHeight)
        locationTextView = createTextView(text, dx: dx + 10 + labelWidth, dy: dy, labelWidth: 100, labelHeight: 30)
    }
    
    func setupProgram(text: String){
        let dx : CGFloat = 20.0
        let dy : CGFloat = 145.0
        
        programLabelView = createLabelView("Program", dx: dx, dy: dy, labelWidth: labelWidth, labelHeight: labelHeight)
        programTextView = createTextView(text, dx: dx + 10 + labelWidth, dy: dy, labelWidth: 100, labelHeight: 30)
        
        setupYearsInProgram()
    }
    
    func setupYearsInProgram(){
        let dx : CGFloat = 20.0
        let dy : CGFloat = 185.0
        
        yearsProgramLabelView = createLabelView("Years in Program", dx: dx, dy: dy, labelWidth: labelWidth, labelHeight: labelHeight)
        yearsProgramSliderView = createSliderView(1, max: 10, dx: dx + 10 + labelWidth, dy: dy, sliderWidth: 100, sliderHeight: 30)
        yearSliderLabel = createTextView("0", dx: dx + 10 + labelWidth + 100, dy: dy, labelWidth: 100, labelHeight: 30)
        yearSliderLabel.textColor = UIColor.blackColor()
        
        yearsProgramSliderView.addTarget (self, action: #selector(numberValueChanged), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func numberValueChanged(sender: UISlider) {
        yearsProgramSliderView.setValue(Float(Int(yearsProgramSliderView.value)), animated: true)
        updateLabels(yearsProgramSliderView.value)
    }
    
    func updateLabels(nV: Float?) {
        if let v = nV {
            yearSliderLabel.text = "\(v)"
        }
    }
    
    func setupGender(){
        let dx : CGFloat = 20.0
        let dy : CGFloat = 225.0
        
        genderLabelView = createLabelView("Gender", dx: dx, dy: dy, labelWidth: labelWidth, labelHeight: labelHeight)
        genderSwitch = UISwitch(frame:CGRectMake(dx + labelWidth + 15, dy, 100, 30));
        genderSwitch.on = true
        genderSwitch.setOn(true, animated: false);
        genderSwitchLabel = createTextView("Male", dx: dx + labelWidth + 75, dy: dy, labelWidth: 100, labelHeight: 30)
        genderSwitchLabel.textColor = UIColor.blackColor()
        genderSwitch.addTarget(self, action: #selector(switchValueDidChange), forControlEvents: .ValueChanged)
    }
    
    func switchValueDidChange(sender: UISwitch) {
        var text : String
        switch(genderSwitch.on){
        case true:
            text = "Male"
        default:
            text = "Female"
        }
        genderSwitchLabel.placeholder = text
    }
    
    func setupLanguages(text: String){
        let dx : CGFloat = 20.0
        let dy : CGFloat = 305.0
        
        languagesLabelView = createLabelView("Languages", dx: dx, dy: dy, labelWidth: labelWidth, labelHeight: labelHeight)
        languagesTextView = createTextView(text, dx: dx + 10 + labelWidth, dy: dy, labelWidth: 100, labelHeight: 30)
    }
    
    func setupInterests(text: String){
        let dx : CGFloat = 20.0
        let dy : CGFloat = 265.0
        
        interestsLabelView = createLabelView("Interests", dx: dx, dy: dy, labelWidth: labelWidth, labelHeight: labelHeight)
        interestsTextView = createTextView(text, dx: dx + 10 + labelWidth, dy: dy, labelWidth: 100, labelHeight: 30)
    }
    
    func setupDegree(text: String){
        let dx : CGFloat = 20.0
        let dy : CGFloat = 345.0
        
        degreeLabelView = createLabelView("Degree", dx: dx, dy: dy, labelWidth: labelWidth, labelHeight: labelHeight)
        degreeTextView = createTextView(text, dx: dx + 10 + labelWidth, dy: dy, labelWidth: 100, labelHeight: 30)
    }
    
    func getCurrentStudent() -> Student{
        let name : String = nameTextView.text!
        let from : String = locationTextView.text!
        let gender : Student.Gender
        switch(genderSwitch.on){
        case true:
            gender = .Male
        default:
            gender = .Female
        }
        
        let program : String = programTextView.text!
        let languages : [String] = languagesTextView.text!.characters.split{$0 == ","}.map(String.init)
        let years : Int = Int(yearsProgramSliderView.value)
        let interests : [String] = interestsTextView.text!.characters.split{$0 == ","}.map(String.init)
        let degree : String = degreeTextView.text!
        let image = photoImageView.image
        let stud = Student(im: image, n: name, loc: from, py: years, d: degree, l: languages, i: interests, g: gender, p: program)
        return stud
    }
    
    func displayProfile(sender: UIButton!){
        let person : Student = getCurrentStudent()
        let description : String = person.description
        let dx : CGFloat = 10.0
        let dy : CGFloat = 385.0
        descriptionLabel = createLabelView(description, dx: dx, dy: dy, labelWidth: screenWidth - 20, labelHeight: 100)
        descriptionLabel?.numberOfLines = 3
        descriptionLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.minimumScaleFactor = 0.1
        descriptionLabel.textColor = UIColor.blackColor()
        descriptionLabel.hidden = false
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        photoImageView.image = selectedImage
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func selectImageGesture(sender: UITapGestureRecognizer) {
        nameTextView.resignFirstResponder()
        locationTextView.resignFirstResponder()
        degreeTextView.resignFirstResponder()
        programTextView.resignFirstResponder()
        languagesTextView.resignFirstResponder()
        interestsTextView.resignFirstResponder()
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .PhotoLibrary
        imagePickerController.delegate = self
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if saveButton === sender {
            student = getCurrentStudent()
            let studentViewController = segue.destinationViewController as! StudentTableViewController
            let cellPath = studentViewController.tableView.indexPathForSelectedRow
            let cellVal = cellPath == nil ? studentViewController.students.count : cellPath!.row
            let indexPath = NSIndexPath(forRow: cellVal, inSection: 0)
            let cell = studentViewController.tableView.dequeueReusableCellWithIdentifier("StudentTableViewCell", forIndexPath: indexPath) as! StudentTableViewCell
            cell.photoImageView.image = student!.image
            cell.nameLabel.text = student!.name
            studentViewController.students[cellVal - 1] = student!
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        saveButton.enabled = false
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        checkValidStudentName()
        navigationItem.title = textField.text
    }
    
    func checkValidStudentName(){
        let name = nameTextView.text ?? ""
        let loc = locationTextView.text ?? ""
        let program = programLabelView.text ?? ""
        let degree = degreeLabelView.text ?? ""
        saveButton.enabled = !name.isEmpty && !loc.isEmpty && !program.isEmpty && !degree.isEmpty
    }
    
}