//
//  DisplayViewController.swift
//  at201-ece590-hw32
//
//  Created by Adam on 9/20/16.
//  Copyright © 2016 Adam. All rights reserved.
//

import UIKit

class DisplayViewController: UIViewController {

    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var photoView: UIImageView!
    var student: Student!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionView.text = student.description
        descriptionView?.numberOfLines = 10
        descriptionView.adjustsFontSizeToFitWidth = true
        descriptionView.minimumScaleFactor = 0.1
        photoView.image = student.image
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "EditDetail" {
            let studentDetailViewController = segue.destinationViewController as! StudentViewController
            studentDetailViewController.student = student
        }
    }

}