//
//  AppDelegate.swift
//  at201-ece590-hw32
//
//  Created by Adam on 9/20/16.
//  Copyright © 2016 Adam. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}