//
//  Human.swift
//  at201-ece590-hw2
//
//  Created by Adam on 9/12/16.
//  Copyright © 2016 Adam. All rights reserved.
//

import UIKit
import Foundation

extension String{
    /* Helper function for capitizalizing first character of String. */
    var first: String{
        return String(characters.prefix(1))
    }
    var uppercaseFirst: String{
        return first.uppercaseString + String(characters.dropFirst())
    }
}

class Student: CustomStringConvertible{
    
    var name: String
    var location: String
    var yearInProgram: Int
    var degree: String
    var languages: [String]
    var interests: [String]
    var pronoun: String
    var genderVerb: String
    var gender: String
    var program: String
    var image: UIImage!
    
    enum Gender{
        case Male, Female, Nonbinary
    }
    
    var description: String {
        var toReturn : String = "\(name) is \(gender) and \(genderVerb) from \(location)."
        if(program != "nothing"){
            toReturn = "\(toReturn) \(pronoun) \(genderVerb) in year \(yearInProgram) of \(program) pursuing \(degree)."
        }
        else{
            toReturn = "\(toReturn) \(pronoun) \(genderVerb) is pursuing no degree."
        }
        if(languages.count > 0){
            toReturn = "\(toReturn) " + pronoun.uppercaseFirst + " programs in " + languages.joinWithSeparator(", ") + "."
        }
        else{
            toReturn = "\(toReturn) \(pronoun) programs in nothing."
        }
        if(interests.count > 0){
            toReturn = "\(toReturn) " + pronoun.uppercaseFirst + " enjoys " + interests.joinWithSeparator(", ")+"."
        }
        else{
            toReturn = "\(toReturn) " + pronoun.uppercaseFirst + " has no interests."
        }
        return toReturn
    }
    
    init(im: UIImage?, n: String, loc: String, py: Int, d: String, l: [String], i: [String], g: Gender, p: String){
        name = n
        location = loc
        program = p
        yearInProgram = py
        degree = d
        languages = l
        interests = i
        image = im
        
        switch g{
        case .Male:
            gender = "male"
            pronoun = "He"
            genderVerb = "is"
        case .Female:
            gender = "female"
            pronoun = "She"
            genderVerb = "is"
        default:
            gender = "nonbinary"
            pronoun = "They"
            genderVerb = "are"
        }
        
    }
    
}