//
//  TeamTableViewCell.swift
//  at201-ece590-hw4
//
//  Created by Adam on 9/23/16.
//  Copyright © 2016 Adam. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var teamLabel: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}